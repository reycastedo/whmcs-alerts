// Declaramos las variables que serán usadas globalmente
var ticketsPendientes = 0;
var ticketsProgreso = 0;

// Obtenemos los nodos con valor Open o Customer-Reply y comprobamos que pertenezcan a una tabla
// Sumamos uno al valor de la variable ticketsPendientes por cada uno de ellos
function obtenerTicketsPendientes() {

    var spanElements = document.querySelectorAll('span') || [];
    
    for ( var i = 0; i < spanElements.length; i++ ) {

        if ( spanElements[i].innerText == "Open" || spanElements[i].innerText == "Abierto" || spanElements[i].innerText == "Customer-Reply" || spanElements[i].innerText == "Respuesta-Cliente" ) {

            // Comprobamos que el nodo padre es un td, evitando así problemas y asegurándonos que pertenece a la tabla correspondiente
            if ( spanElements[i].parentNode.nodeName == "TD" ) {

                ticketsPendientes++;
            }
        }
    }
}

// Obtenemos los nodos con valor In Progress y comprobamos que pertenezcan a una tabla y el siguiente nodo cumpla un patrón definido
// Sumamos uno al valor de la variable ticketsProgreso por cada uno de ellos
function responderTickets() {

    var spanElements = document.querySelectorAll('span') || [];
    var pattern1 = /[1-9]+h/;
    var pattern2 = /5[0-9]m/;


    for ( var i = 0; i < spanElements.length; i++ ) {

        if ( spanElements[i].innerText == "In Progress" || spanElements[i].innerText == "En Progreso" ) {

            // Comprobamos que el nodo padre es un TD
            if ( spanElements[i].parentNode.nodeName == "TD" ) {

                // Comprobamos que el ticket está 5X minutos o por encima
                if ( spanElements[i].parentNode.nextSibling.innerText.match(pattern1) || spanElements[i].parentNode.nextSibling.innerText.match(pattern2) ) {

                    ticketsProgreso++;
                }
            }
        }
    }
}

// Si el valor de alguna de las variables globales comprobadas en funciones anteriores es superior a 0, emite un sonido
function beep() {
    if ( ticketsPendientes > 0  ||  ticketsProgreso > 0 ) {

        var sound = new Audio( browser.extension.getURL( 'assets/alert.mp3' ) );
        sound.play();
        console.log("done");
    }
}

// Lógica principal
function main() {
    obtenerTicketsPendientes();
    responderTickets();

    //console.log(ticketsPendientes);
    //console.log(ticketsProgreso);

    setTimeout(beep, 30000);
}


main();